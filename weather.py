import sopel.module
import urllib2
import json

def getweather(location):
    print location
    f = urllib2.urlopen('http://api.wunderground.com/api/69aa8ef9bb7f24d5/forecast10day/q/' + location + '.json')
    json_string = f.read()
    parsed_json = json.loads(json_string)
    ##text = parsed_json['simpleforecast']['forecastday']
    f.close()
    return "Current temperature in %s is: %s" % (location, parsed_json)


def localSearch(searchText):
    f = urllib2.urlopen('http://autocomplete.wunderground.com/aq?query=' + searchText)
    json_string = f.read()
    parsed_json = json.loads(json_string)
    print parsed_json
    text =  "I found " + str(len(parsed_json["RESULTS"])) +" locations for that search "
    count = 0
    for i in parsed_json["RESULTS"]:
        count = count +1
        text += str(count) + ") " + i["name"] + " " + i["zmw"] + "    "
    f.close()
    return text
@sopel.module.commands('weather')
def helloworld(bot,trigger):
    print trigger
    bot.say(getweather(trigger.group(2)))

@sopel.module.commands('localSearch')
def search(bot, trigger):
    bot.say(localSearch(trigger.group(2)))

